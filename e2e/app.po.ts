import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getTitle() {
    return element(by.css('app-header mat-toolbar h1')).getText();
  }
  enterUsername(username: string) {
    return element(by.name('email')).sendKeys(username);
  }
  enterPassword(password: string) {
    return element(by.name('password')).sendKeys(password);
  }
  submitLoginForm() {
    return element(by.css('button[type="submit"]')).click();
  }

  getUserProfileName() {
    return element(by.css('app-profile .username')).getText();
  }
}
