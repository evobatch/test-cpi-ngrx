import { AppPage } from './app.po';
import { browser } from 'protractor';

describe('test-cpi App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getTitle()).toEqual('My Book Library For Crane PI');
  });

  it('should have title "Test Crane PI"', () => {
    page.navigateTo();
    browser.getTitle()
      .then((title: string) => {
        expect(title).toEqual('Test Crane PI');
        console.log(title);
      });
  });
  it('should loggin user', () => {
    page.navigateTo();
    page.enterUsername('username');
    page.enterPassword('password');
    page.submitLoginForm();
    browser.waitForAngular().then(() => {
      console.log(page.getUserProfileName());
      expect(page.getUserProfileName()).toEqual('username');
    });
  });
});
