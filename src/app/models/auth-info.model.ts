export interface IAuthInfo {
  email: string;
  token: string;
}
