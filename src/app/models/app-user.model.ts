export interface IAppUser {
  name: string;
  email: string;
}
