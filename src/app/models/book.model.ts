export interface IBook {
  id: number;
  name: string;
  year: number;
  color: string;
}
