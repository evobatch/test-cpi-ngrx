import { Directive } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { MaterialModule } from 'app/shared/material-module';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: 'app-profile'
}) class MockAppProfileDirective { }


describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HeaderComponent,
        MockAppProfileDirective],
        imports: [
          MaterialModule,
          RouterTestingModule.withRoutes([])
        ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    router = TestBed.get(Router);
    spyOn(router, 'navigate').and.stub();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call "/" url', () => {
    component.home();
    expect(router.navigate).toHaveBeenCalledTimes(1);
    expect(router.navigate).toHaveBeenCalledWith(['/']);
  });

  it('should call "/books"', () => {
    component.gotoBookList();
    expect(router.navigate).toHaveBeenCalledTimes(1);
    expect(router.navigate).toHaveBeenCalledWith(['/books']);
  });

});
