import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IBook } from 'app/models/book.model';

import * as fromBook from './book.actions';
import { LoadFail, LoadSuccess, FilterPage, BookActions } from './book.actions';

/**
 * interface defining the structure of Book State.
*/
export interface BookState extends PagingInfo {
  bookList: IBook[];
  filterText?: string;
  filteredPage?: IBook[];
  error?: string;
}

export interface PagingInfo {
  currentPage?: number;
  totalBookCount?: number;
  totalPageCount?: number;
}

export const initialState: BookState = {
  bookList: []
};

/**
 * Book main reducer function.
 * @param state current application state
 * @param action triggered action
 */
export function reducer(state: BookState = initialState, action: BookActions): BookState {
  switch (action.type) {
    case fromBook.BookActionTypes.Load:
      return {
        ...state,
        currentPage: action.payload || 1
      };
    case fromBook.BookActionTypes.LoadSuccess:
      return reduceLoadSuccess(state, action);
    case fromBook.BookActionTypes.LoadFail:
      return {
        ...state,
        error: JSON.stringify(action.payload)
      };
    case fromBook.BookActionTypes.FilterPage:
      return reduceFilterPage(state, action);
    default:
      return state;
  }
}

function reduceLoadSuccess(state: BookState, action: LoadSuccess): BookState {
  return {
    ...state,
    bookList: action.payload.data,
    filteredPage: action.payload.data,
    filterText: '',
    currentPage: action.payload.page,
    totalBookCount: action.payload.total,
    totalPageCount: action.payload.total_pages
  };
}

function reduceFilterPage(state: BookState, action: FilterPage): BookState {
  const search = action.payload.toUpperCase();
  const filteredList = search ? state.bookList.filter(b =>  b.name.toUpperCase().indexOf(search) > -1) : state.bookList;
  return {
    ...state,
    filterText: action.payload,
    filteredPage: filteredList
  };
}

/**
 * Project book state from whole application state.
*/
export const selectBookState = createFeatureSelector<BookState>('books');

/**
 * Project Filtered Page collection from book state into an observable.
*/
export const getFilteredPage = createSelector(
  selectBookState,
  (state: BookState) =>  state.filteredPage || state.bookList
);

/**
 * select `filterText` from book state as an observable.
*/
export const getFilterText = createSelector(
  selectBookState,
  (state: BookState) =>  state.filterText
);

/**
 * Project PageInfo from book state into an observable.
*/
export const getCurrentPageInfo = createSelector(
  selectBookState,
  (state: BookState) => <PagingInfo>{
    currentPage: state.currentPage,
    totalBookCount: state.totalBookCount,
    totalPageCount: state.totalPageCount
  }
);
