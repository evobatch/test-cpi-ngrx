import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Actions } from '@ngrx/effects';
import { IBook } from 'app/models/book.model';
import { getActions, TestActions } from 'app/shared/test-utils';
import { cold, hot } from 'jasmine-marbles';

import { Load, LoadSuccess } from './book.actions';
import { BookEffects } from './book.effects';
import { BookService } from './services/book.service';

describe('Book Effects ', () => {
  let effects: BookEffects;
  let actions$: TestActions;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        BookEffects,
        { provide: Actions, useFactory: getActions },
        { provide: BookService, useValue: { query: () => cold('-a|', { a: payload })} }
      ]
    });

    actions$ = TestBed.get(Actions);
    effects = TestBed.get(BookEffects);
  });

  it('Load effect should call loadSuccess', () => {
    actions$.stream = hot('a', { a: new Load(1) });
    const expected = cold('-c', { c: new LoadSuccess(payload) });
    expect(effects.load$).toBeObservable(expected);

  });
});

const bookList = <IBook[]>[
  {
    id: 7,
    name: 'sand dollar',
    year: 2006,
    color: '#DECDBE'
  },
  {
    id: 8,
    name: 'chili pepper',
    year: 2007,
    color: '#9B1B30'
  }
];

const payload = {
  data: bookList,
  page: 1,
  per_page: 2,
  total: 4,
  total_pages: 2
};
