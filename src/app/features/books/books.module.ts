import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { BookService } from 'app/features/books/services/book.service';
import { MaterialModule } from 'app/shared/material-module';

import { BookListComponent } from './book-list/book-list.component';
import { BookEffects } from './book.effects';
import * as fromBook from './book.reducers';
import { BooksRoutingModule } from './books-routing.module';

const booksComponents = [
  BookListComponent
];

@NgModule({
  imports: [
    MaterialModule,
    CommonModule,
    FlexLayoutModule,
    FormsModule
  ],
  declarations: booksComponents,
  exports: booksComponents
})
export class BooksModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: BooksRootModule,
      providers: [BookService]
    };
  }
}
@NgModule({
  imports: [
    BooksModule,
    BooksRoutingModule,
    StoreModule.forFeature('books', fromBook.reducer),
    EffectsModule.forFeature([BookEffects])
  ]
})
/**
 * Module responsible to manage books.
 * Use `BooksModule.forRoot()` to register it to main application along with BookService.
 */
export class BooksRootModule { }
