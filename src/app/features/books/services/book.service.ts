import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs/Observable';
import { IBook } from 'app/models/book.model';

export interface IBookPayload {
  data: IBook[];
  page: number;
  per_page: number;
  total: number;
  total_pages: number;

}
@Injectable()
export class BookService {
  url = `${environment.apiRoot}/books`;
  constructor(
    private http: HttpClient
  ) { }

  query(page?: number): Observable<IBookPayload> {
    const params = page ?
      new HttpParams().append('page', page.toString()) :
      new HttpParams();
    return this.http.get<IBookPayload>(this.url, { params: params });
  }

}
