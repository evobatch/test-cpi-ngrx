import 'rxjs/add/operator/toPromise';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';

import { BookService } from './book.service';

describe('BookService', () => {
  let bookService: BookService;
  let backend: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [BookService]
    });
    bookService = TestBed.get(BookService);
    backend = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    backend.verify();
  });

  it('should be created', inject([BookService], (service: BookService) => {
    expect(service).toBeTruthy();
  }));

  it('should retrieve all books', (done) => {
    bookService.query().toPromise().then((bookList) => {
      expect(bookList.data.length).toBeGreaterThan(1);
      expect(bookList.page).toBe(1);
      done();
    }, () => fail());

    const payload = {
      data: [
        { id: 1, name: 'cerulean', year: 2000, color: '#98B2D1', pantone_value: '15 - 4020' },
        { id: 2, name: 'fuchsia rose', year: 2001, color: '#C74375', pantone_value: '17 - 2031' },
        { id: 3, name: 'true red', year: 2002, color: '#BF1932', pantone_value: '19 - 1664' }
      ],
      page: 1,
      per_page: 3,
      total: 12,
      total_pages: 4
    };

    const request = backend.expectOne({
      url: bookService.url,
      method: 'GET'
    });
    request.flush(payload);
  });
});
