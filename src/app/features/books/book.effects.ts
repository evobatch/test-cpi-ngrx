import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/do';

import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as fromBook from './book.actions';
import { BookService } from './services/book.service';

@Injectable()
export class BookEffects {

  @Effect()
  load$: Observable<Action> = this.actions$
    .ofType<fromBook.Load>(fromBook.BookActionTypes.Load)
    .map(action => action.payload)
    .switchMap(page => {
      return this.bookService.query(page)
        .map(result => new fromBook.LoadSuccess(result) )
        .do(x => {

        })
        .catch(error => Observable.of(new fromBook.LoadFail(error)));
    });


  constructor(
      private actions$: Actions,
      private bookService: BookService
  ) { }

}
