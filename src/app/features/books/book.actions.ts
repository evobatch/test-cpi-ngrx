import { Action } from '@ngrx/store';

import { IBookPayload } from './services/book.service';

export enum BookActionTypes {
  Load = '[Book] Load',
  LoadSuccess = '[Book] Load Success',
  LoadFail = '[Book] Load Fail',
  FilterPage = '[Book] Filter Page'
}

export class Load implements Action {
  readonly type = BookActionTypes.Load;
  /**
   * Load Book Page
   * @param payload optional page number
   */
  constructor(public payload?: number) {}
}

export class LoadSuccess implements Action {
  readonly type = BookActionTypes.LoadSuccess;
  /**
   * Load Book Page Success Action
   * @param payload list of books and paging info
   */
  constructor(public payload: IBookPayload) {}
}

export class LoadFail implements Action {
  readonly type = BookActionTypes.LoadFail;
  /**
   * Load Book Page failed Action
   * @param payload failed reason
   */
  constructor(public payload: any) {}
}


export class FilterPage implements Action {
  readonly type = BookActionTypes.FilterPage;
  /**
   * Filter Current Page Action
   * @param payload filterText
   */
  constructor(public payload: string) {}
}

export type BookActions = Load | LoadSuccess | LoadFail | FilterPage;
