import { Routes, RouterModule } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { BookListComponent } from './book-list/book-list.component';
import { NgModule } from '@angular/core';

/**
 * routes for books module
 */
export const routes: Routes = [
  { path: 'books', component: BookListComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
/**
 * Module containing books module routing info
 */
export class BooksRoutingModule { }


