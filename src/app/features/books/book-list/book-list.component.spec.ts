import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';

import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Store } from '@ngrx/store';
import { MaterialModule } from 'app/shared/material-module';
import { MockStore } from 'app/shared/test-utils';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import { BookListComponent } from './book-list.component';
import { Subject } from 'rxjs/Subject';
import { BookState, PagingInfo } from '../book.reducers';
import { Load, FilterPage } from '../book.actions';

describe('BookListComponent', () => {
  let component: BookListComponent;
  let fixture: ComponentFixture<BookListComponent>;
  const selection: Subject<Array<any>> = new BehaviorSubject([{name: 'first'}]);
  let store: Store<BookState>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookListComponent ],
      imports: [
        MaterialModule,
        NoopAnimationsModule,
        FormsModule],
        providers: [
          {provide: Store, useClass: MockStore },
        ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookListComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);
    spyOn(store, 'select').and.callFake(() => selection);
    component.ngOnInit();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have "List of Books" title', fakeAsync(() => {
      component.bookList$.take(1).subscribe(() => {
          const bookTitleEl = fixture.debugElement.query(By.css('mat-card mat-card-title'));
          expect(bookTitleEl.nativeElement.textContent).toBe('List of Books');
      }, () => fail());
  }));

  it('should call dispatch load', () => {
    const spyDispatch = spyOn(store, 'dispatch').and.callThrough();
    component.first();
    expect(store.dispatch).toHaveBeenCalledWith(new Load());
  });

  it('next() should call dispatch load(2)', () => {
    const spyDispatch = spyOn(store, 'dispatch').and.callThrough();
    component.pageInfo = <PagingInfo>{ currentPage: 1, totalPageCount: 12, totalBookCount: 5};
    component.next();
    expect(store.dispatch).toHaveBeenCalledWith(new Load(2));
    expect(store.dispatch).toHaveBeenCalledTimes(1);
  });

  it('previous() should call dispatch load(1)', () => {
    const spyDispatch = spyOn(store, 'dispatch').and.callThrough();
    component.pageInfo = <PagingInfo>{ currentPage: 2, totalPageCount: 12, totalBookCount: 5};
    component.previous();
    expect(store.dispatch).toHaveBeenCalledWith(new Load(1));
    expect(store.dispatch).toHaveBeenCalledTimes(1);
  });

  it('previous() limit to first', () => {
    const spyDispatch = spyOn(store, 'dispatch').and.callThrough();
    component.pageInfo = <PagingInfo>{ currentPage: 0, totalPageCount: 12, totalBookCount: 5};
    component.previous();
    expect(store.dispatch).toHaveBeenCalledWith(new Load());
    expect(store.dispatch).toHaveBeenCalledTimes(1);
  });

  it('next() limit to maxPage', () => {
    const spyDispatch = spyOn(store, 'dispatch').and.callThrough();
    component.pageInfo = <PagingInfo>{ currentPage: 12, totalPageCount: 12, totalBookCount: 5};
    component.next();
    expect(store.dispatch).toHaveBeenCalledWith(new Load(12));
    expect(store.dispatch).toHaveBeenCalledTimes(1);
  });

  it('filterPage() should call dispatch', () => {
    const spyDispatch = spyOn(store, 'dispatch').and.callThrough();
    component.filterPage('test');
    expect(store.dispatch).toHaveBeenCalledWith(new FilterPage('test'));
    expect(store.dispatch).toHaveBeenCalledTimes(1);
  });
});
