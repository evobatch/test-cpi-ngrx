import 'rxjs/add/operator/takeWhile';

import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material';
import { Store } from '@ngrx/store';
import { IBook } from 'app/models/book.model';
import { Observable } from 'rxjs/Observable';

import * as fromBook from '../book.actions';
import { BookState, getCurrentPageInfo, getFilteredPage, getFilterText, PagingInfo } from '../book.reducers';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit, OnDestroy, AfterViewInit {
  pageInfo: PagingInfo;
  displayedColumns = [ 'name', 'year'];
  bookList$: Observable<IBook[]>;
  destroy = false;
  filterText = '';

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private store: Store<BookState>
  ) { }

  ngOnInit() {

    // load first page
    this.first();

    // get booklist observable
    this.bookList$ = this.store
      .select(getFilteredPage)
      .takeWhile(() => !this.destroy);

    // subscribe to page info
    this.store
      .select(getCurrentPageInfo)
      .takeWhile(() => !this.destroy)
      .subscribe(pageInfo => this.pageInfo = pageInfo);

      // get
      this.store.select(getFilterText)
      .takeWhile(() => !this.destroy)
      .subscribe(text => this.filterText = text);
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    this.destroy = true;
  }


  next() {
    let page = (this.pageInfo.currentPage || 0) + 1;
    if (page > this.pageInfo.totalPageCount) {
      page = this.pageInfo.totalPageCount;
    }
    this.store.dispatch(new fromBook.Load(page));
  }

  first() {
    this.store.dispatch(new fromBook.Load());
  }

  previous() {
    let page = (this.pageInfo.currentPage || 0) - 1;
    if (page < 1) {
      page = undefined;
    }
    this.store.dispatch(new fromBook.Load(page));
  }

  filterPage(searchTerm) {
    this.store.dispatch(new fromBook.FilterPage(searchTerm));
  }
}
