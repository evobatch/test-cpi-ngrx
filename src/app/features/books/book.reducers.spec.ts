import { initialState, reducer, BookState } from './book.reducers';
import { Load, LoadSuccess, LoadFail, FilterPage } from './book.actions';
import { IBook } from 'app/models/book.model';

describe('books reducer', () => {

it('should return default state', () => {
  const result = reducer(undefined, <any>{});
  expect(result).toBe(initialState);
});

  it('should start loading books page 1', () => {
    const result = reducer(initialState, new Load());
    expect(result.currentPage).toBe(1);
  });

  it('should load successfully books, paging info and should reset filterText', () => {
    const bookList = <IBook[]>[
      {
        id: 7,
        name: 'sand dollar',
        year: 2006,
        color: '#DECDBE'
      },
      {
        id: 8,
        name: 'chili pepper',
        year: 2007,
        color: '#9B1B30'
      }
    ];

    const payload = {
      data: bookList,
      page: 1,
      per_page: 2,
      total: 4,
      total_pages: 2
    };
    const result = reducer(initialState, new LoadSuccess(payload));
    expect(result.bookList.length).toBe(2);
    expect(result.currentPage).toBe(payload.page);
    expect(result.totalBookCount).toBe(payload.total);
    expect(result.totalPageCount).toBe(payload.total_pages);
    expect(result.filterText).toBeFalsy();
  });

  // todo (yca): define error handling depending on server responses.
  xit('should failed to load books.', () => {
    const error = {
      error: 'unknow reason',
      status: 500
    };
    const result = reducer(initialState, new LoadFail(error.error));
  });

  it('should filter current page', () => {
    const state = <BookState>{
      bookList: [
        {
          id: 7,
          name: 'sand dollar',
          year: 2006,
          color: '#DECDBE'
        },
        {
          id: 8,
          name: 'chili pepper',
          year: 2007,
          color: '#9B1B30'
        }
      ],
      filteredPage: null,
      currentPage: 3,
      filterText: '',
      totalBookCount: 12,
      totalPageCount: 4
    };
    const result = reducer(state, new FilterPage('nd'));
    expect(result.filterText).toBe('nd');
      expect(result.filteredPage).toEqual(
      [{
          id: 7,
          name: 'sand dollar',
          year: 2006,
          color: '#DECDBE'
        }]);
  });



});
