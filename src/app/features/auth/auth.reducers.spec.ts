import { LoginAction, LoginSuccessAction, LoginErrorAction, LogoutAction, LogoutSuccessAction, LogoutErrorAction } from './auth.actions';
import { reducer } from './auth.reducers';
import * as fromAuth from './auth.reducers';
import { ILoginCredentials } from './models/credentials.model';
import { IAuthInfo } from '../../models/auth-info.model';


describe('auth reducer', () => {
  it('should return default state on undefined state', () => {
    const action = {} as any;
    const result = reducer(undefined, action);
    expect(result).toEqual(fromAuth.initialState);
  });

  describe('login', () => {
    it('login should set pending', () => {
      const credentials = <ILoginCredentials>{
        email: '',
        password: ''
      };
      const loginAction = new LoginAction(credentials);
      const result = reducer(fromAuth.initialState, loginAction);
      expect(result.pending).toBe(true);
    });

    it('login success should clear pending, set authenticated, add email and token, clear error', () => {
      const authInfo = <IAuthInfo>{
        email: 'test@test.co',
        token: '12esdfsdfsdf'
      };
      const state = {
        authenticated: false,
        pending: true,
        error: 'error invalid password'
      };
      const loginSuccessAction = new LoginSuccessAction(authInfo);
      const result = reducer(state, loginSuccessAction);
      expect(result.pending).toBeFalsy();
      expect(result.error).toBeFalsy();
      expect(result.authenticated).toBe(true);
      expect(result.email).toBe(authInfo.email);
      expect(result.token).toBe(authInfo.token);
    });


    it('login failed should clear pending, add error', () => {
      const error = {
        error: { error: 'something went wrong' },
        status: 400
      };
      const state = {
        authenticated: null,
        pending: true,
        error: null
      };
      const loginErrorAction = new LoginErrorAction(error);
      const result = reducer(state, loginErrorAction);
      expect(result.pending).toBeFalsy();
      expect(result.error).toEqual(error.error.error);
      expect(result.authenticated).toBe(false);
      expect(result.email).toBeFalsy();
      expect(result.token).toBeFalsy();
    });
  });

  describe('logout', () => {
    it('logout should set pending', () => {
      const loggedInstate = {
        authenticated: true,
        pending: false,
        email: 'test@test.com',
        token: 'asddsgdghgkjhjz'
      };
      const logoutAction = new LogoutAction();
      const result = reducer(loggedInstate, logoutAction);
      expect(result.pending).toBe(true);
    });

    it('logout success should clear pending, authenticated, email, token and error', () => {
      const loggoutstate = {
        authenticated: true,
        pending: true,
        email: 'test@test.com',
        error: 'errorsdd  sdfsdf sdf',
        token: 'asddsgdghgkjhjz'
      };
      const logoutSuccessAction = new LogoutSuccessAction();
      const result = reducer(loggoutstate, logoutSuccessAction);
      expect(result.pending).toBeFalsy();
      expect(result.authenticated).toBeFalsy();
      expect(result.email).toBeFalsy();
      expect(result.token).toBeFalsy();
      expect(result.error).toBeFalsy();
    });

    it('logout failed should clear pending, and set error', () => {
      const loggoutstate = {
        authenticated: true,
        pending: true,
        email: 'test@test.com',
        error: null,
        token: 'asddsgdghgkjhjz'
      };
      const logoutErrorAction = new LogoutErrorAction();
      const result = reducer(loggoutstate, logoutErrorAction);
      expect(result.pending).toBeFalsy();
      expect(result.authenticated).toBe(true);
      expect(result.email).toBe('test@test.com');
      expect(result.token).toBe('asddsgdghgkjhjz');
      expect(result.error).toBeTruthy();
    });

  });

});
