import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { AuthActionTypes } from './auth.actions';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import * as fromAuth from './auth.actions';
import { LoginService } from './services/login.service';
import { Router } from '@angular/router';

@Injectable()
export class AuthEffects {

  @Effect()
  login$ = this.actions$
    .ofType<fromAuth.LoginAction>(fromAuth.AuthActionTypes.Login)
    .map(action => action.payload)
    .switchMap((data) => {
      return this.loginService.login(data.email, data.password)
        .map(auth => new fromAuth.LoginSuccessAction({ email: data.email, token: auth.token }))
        .catch(error => Observable.of(new fromAuth.LoginErrorAction(error)));
    });

  @Effect({ dispatch: false })
  loginSuccess$ = this.actions$
    .ofType<fromAuth.LoginSuccessAction>(fromAuth.AuthActionTypes.LoginSuccess)
    .do(() =>  this.router.navigate(['/']));

  @Effect()
  logout$ = this.actions$
    .ofType<fromAuth.LogoutAction>(fromAuth.AuthActionTypes.Logout)
    .switchMap((data) => {
      return this.loginService.logout()
        .map(auth => new fromAuth.LogoutSuccessAction())
        .catch(error => Observable.of(new fromAuth.LogoutErrorAction(error)));
    });

  @Effect({ dispatch: false })
  loginRedirect$ = this.actions$
    .ofType<fromAuth.LogoutSuccessAction>(fromAuth.AuthActionTypes.LogoutSuccess)
    .do(() => {
      this.router.navigate(['/login']);
    });

  constructor(
    private actions$: Actions,
    private router: Router,
    private loginService: LoginService) {
  }

}
