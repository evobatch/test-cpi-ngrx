import { IAppUser } from 'app/models/app-user.model';
import * as fromAuth from './auth.actions';
import { IAuthInfo } from 'app/models/auth-info.model';
import { createSelector, createFeatureSelector } from '@ngrx/store';

export interface AuthState {
  authenticated: boolean;
  pending: boolean;
  token?: string;
  email?: string;
  error?: string;
}

export const initialState: AuthState = {
  authenticated: null,
  pending: false
};

export function reducer(state: AuthState = initialState, action: fromAuth.AuthActions): AuthState {

  switch (action.type) {
    case fromAuth.AuthActionTypes.Login:
      return {
        ...state,
        pending: true
      };
    case fromAuth.AuthActionTypes.LoginSuccess:
      return {
        ...state,
        email: action.payload.email,
        token: action.payload.token,
        error: null,
        pending: false,
        authenticated: true
      };
    case fromAuth.AuthActionTypes.LoginError:
      return reduceLoginErrorAction(state, action);
    case fromAuth.AuthActionTypes.Logout:
      return {
        ...initialState,
        pending: true
      };
    case fromAuth.AuthActionTypes.LogoutSuccess:
      return {
        ...initialState,
        pending: false
      };
    case fromAuth.AuthActionTypes.LogoutError:
      return {
        ...state,
        error: JSON.stringify(action.payload) || 'unable to logout',
        pending: false
      };
    default:
      return state;
  }
}

function reduceLoginErrorAction(state: AuthState = initialState, action: fromAuth.LoginErrorAction) {
  let err = JSON.stringify(action.payload);
  if (action.payload.status === 400 && !!action.payload.error) {
    err = action.payload.error.error;
  }
  return {
    ...state,
    error: err,
    pending: false,
    authenticated: false
  };
}



export const selectAuthState = createFeatureSelector<AuthState>('auth');

export const getAuthenticated = createSelector(
  selectAuthState,
  (state: AuthState) => state.authenticated
);

export const getAuthenticationError = createSelector(
  selectAuthState,
  (state: AuthState) => state.error
);

export const getUser = createSelector(
  selectAuthState,
  (state: AuthState) => <IAppUser>{ email: state && state.email, name: state && state.email }
);
