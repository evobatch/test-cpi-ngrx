import { Action } from '@ngrx/store';
import { IAppUser } from 'app/models/app-user.model';
import { IAuthInfo } from 'app/models/auth-info.model';

/**
 * Auth Action Types Enum
*/
export enum AuthActionTypes {
  Login = '[auth] Login',
  LoginSuccess = '[auth] Login success',
  LoginError = '[auth] Login error',
  LoginRedirect = '[auth] Login Redirect',
  Logout = '[auth] Logout out',
  LogoutError = '[auth] Logout error',
  LogoutSuccess = '[auth] Logout success'
}

/**
 * Login Action
 */
export class LoginAction implements Action {
  public readonly type = AuthActionTypes.Login;

/**
 * Login Action
 * @param payload email and password
 */
  constructor(public payload: {email: string, password: string}) {}
}


export class LoginSuccessAction implements Action {
  readonly type = AuthActionTypes.LoginSuccess;

  /**
   * Login Success Action ctor
   * @param payload authInfo
   */
  constructor(public payload:  IAuthInfo) {}
}

export class LoginErrorAction implements Action {
  readonly type = AuthActionTypes.LoginError;
  /**
   * Login Error Action ctor
   * @param payload error returning by request
   */
  constructor(public payload?: any) {}
}

export class LogoutAction implements Action {
  readonly type = AuthActionTypes.Logout;
  /**
   * Logout Action ctor
   */
  constructor() {}
}

export class LogoutSuccessAction implements Action {
  readonly type = AuthActionTypes.LogoutSuccess;
    /**
     * Logout Success ctor
     */
  constructor() {}
}

export class LogoutErrorAction implements Action {
  readonly type = AuthActionTypes.LogoutError;
  /**
   * Logout Error Action ctor
   * @param payload error returning by request
   */
  constructor(public payload?: any) {}
}

export type AuthActions =
  LoginAction |
  LoginSuccessAction |
  LoginErrorAction |
  LogoutAction |
  LogoutSuccessAction |
  LogoutErrorAction;
