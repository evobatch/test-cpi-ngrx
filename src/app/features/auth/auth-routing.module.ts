import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// guards
import { AuthGuard } from './services/auth.guard';

// components
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';

// routes
const routes: Routes = [
  { path: 'my-account', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent }
];

/**
 * responsible to register routes for `Auth` child module
 */
@NgModule({
  exports: [
    RouterModule
  ],
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class AuthRoutingModule { }
