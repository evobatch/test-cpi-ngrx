import { DebugElement } from '@angular/core';
import {
  async,
  ComponentFixture,
  ComponentFixtureAutoDetect,
  fakeAsync,
  TestBed,
  tick
} from '@angular/core/testing';
import { FormsModule, NgForm } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { combineReducers, Store, StoreModule } from '@ngrx/store';
import * as fromRoot from 'app/app.reducers';
import { MaterialModule } from 'app/shared/material-module';

import * as fromAuthActions from '../auth.actions';
import * as fromAuth from '../auth.reducers';
import { LoginComponent } from './login.component';
import { element } from 'protractor';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/take';
import { MockStore } from '../../../shared/test-utils';
import { AuthState } from '../auth.reducers';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Router } from '@angular/router';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
   let form: NgForm;
   let store: Store<AuthState>;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LoginComponent
      ],
      imports: [
        MaterialModule,
        NoopAnimationsModule,
        FormsModule
      ],
      providers: [
        {provide: Store, useClass: MockStore }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    store = TestBed.get(Store);
    (<any>store).mockState('auth', {error: 'test'});
    component = fixture.componentInstance;
    component.ngOnInit();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      form = fixture.debugElement.query(By.directive(NgForm)).injector.get(NgForm);
    });
  });

  it('should have a defined component', () => {
    expect(component).toBeDefined();
  });

  // todo (yca): restore required attribute on password control
  xit('form should be invalid when empty', (done: DoneFn) => {
    component.credentials = { email: '', password: '' };
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const emailCtrl = form.control.get('email');
      const passwordCtrl = form.control.get('password');
      expect(emailCtrl.valid).toBe(false);
      expect(passwordCtrl.valid).toBe(false);
      expect(form.valid).toBeFalsy();
      done();
    });

  });

  /**
 * Set Form to an invalid state and check button disabled attribute
 */
  it('should disabled send button if form invalid', (done) => {
    fixture.whenStable().then(() => {
      const emailCtrl = form.control.get('email');
      const passwordCtrl = form.control.get('password');
      emailCtrl.setValue('');
      passwordCtrl.setValue('');
      fixture.detectChanges();
      const button = fixture.debugElement.query(By.css('button[type="submit"]'));
      expect(button.nativeElement.disabled).toBe(true);
      done();
    });
  });

/**
 * Set Form to a valid state and check button disabled attribute
 */
  it('should enable send button if form valid', (done) => {
    fixture.whenStable().then(() => {
      const emailCtrl = form.control.get('email');
      const passwordCtrl = form.control.get('password');
      emailCtrl.setValue('test user');
      passwordCtrl.setValue('test password');
      fixture.detectChanges();
      const button = fixture.debugElement.query(By.css('button[type="submit"]'));
      expect(button.nativeElement.disabled).toBe(false);
      done();
    });
  });

  // todo (yca) add test for login error
  xit('should display a login error message', (done: DoneFn) => {

  });

  it('should call dispatch on login', (done) => {
    spyOn(store, 'dispatch').and.callThrough();
    const credentials = { email: 'email@test.com', password: '123456Alpha' };
    const action = new fromAuthActions.LoginAction(credentials);
    component.credentials = credentials;
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      component.login();
      expect(store.dispatch).toHaveBeenCalledWith(action);
      done();
    }, () => fail());

  });
});
