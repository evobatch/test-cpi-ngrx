import 'rxjs/add/operator/takeWhile';

import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { LoginAction } from '../auth.actions';
import { AuthState } from '../auth.reducers';
import * as fromAuth from '../auth.reducers';
import { ILoginCredentials } from '../models/credentials.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  destroy = false;
  loginError$: Observable<string>;
  loading$: Observable<boolean>;
  credentials = <ILoginCredentials>{ email: '', password: '' };

  @ViewChild('form') form: NgForm;

  constructor(
     private store: Store<AuthState>
  ) { }

  ngOnInit() {

    this.loginError$ = this.store
      .select(fromAuth.getAuthenticationError)
      .takeWhile(() => !this.destroy);

    this.loading$ = this.store
      .select(fromAuth.selectAuthState)
      .map(s => s.pending)
      .takeWhile(() => !this.destroy);
  }

  ngOnDestroy() {
    this.destroy = true;
  }

  login() {
      this.store.dispatch(new LoginAction(this.form.value));
  }

}
