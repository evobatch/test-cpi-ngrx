import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FlexLayoutModule} from '@angular/flex-layout';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';
import { MaterialModule } from 'app/shared/material-module';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from './auth.effects';
import { LoginService } from './services/login.service';
import { AuthGuard } from './services/auth.guard';
import { AuthRoutingModule } from './auth-routing.module';
import { StoreModule } from '@ngrx/store';
import { reducer } from './auth.reducers';
import { FormsModule } from '@angular/forms';

const authComponents = [
  ProfileComponent,
  LoginComponent
];

@NgModule({
  imports: [
    MaterialModule,
    CommonModule,
    FlexLayoutModule,
    FormsModule
  ],
  declarations: authComponents,
  exports: authComponents
})
export class AuthModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AuthRootModule,
      providers: [LoginService, AuthGuard]
    };
  }
}

@NgModule({
  imports: [
    AuthModule,
    AuthRoutingModule,
    StoreModule.forFeature('auth', reducer),
    EffectsModule.forFeature([AuthEffects])
  ],
  exports: [ProfileComponent]
})
export class AuthRootModule { }
