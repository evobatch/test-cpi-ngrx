import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { AuthEffects } from './auth.effects';
import { LoginService } from './services/login.service';
import { Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { TestActions, getActions } from 'app/shared/test-utils';
import { cold, hot } from 'jasmine-marbles';
import { LoginAction, LoginSuccessAction } from './auth.actions';
import 'rxjs/add/operator/debounceTime';


describe('AuthActions', () => {
  const payload = { token: 'QpwL5tke4Pnpja7X', email: 'test@test.com'};
  const mockLoginService = {
    login: () => cold('-a|', { a: { token: payload.token } }),
    logout: () => cold('-a|', { a: true }) };
  const mockRouter = { navigate: () => null };

  let effects: AuthEffects;
  let actions$: TestActions;
  let routerService: any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthEffects,
        { provide: Router, useValue: mockRouter },
        { provide: Actions, useFactory: getActions },
        { provide: LoginService, useValue: mockLoginService }
      ]
    });

    effects = TestBed.get(AuthEffects);
    actions$ = TestBed.get(Actions);
    routerService = TestBed.get(Router);

    spyOn(routerService, 'navigate').and.callThrough();

  });

  it('login effect should call loginSuccess', () => {
    actions$.stream = hot('a', { a: new LoginAction({ email: payload.email, password: 'asdasd' }) });
    const expected = cold('-c', { c: new LoginSuccessAction(payload) });
    expect(effects.login$).toBeObservable(expected);
  });

 // todo (yca):Need to use scheduler
  xit('loginSuccess should redirect', fakeAsync(() => {
    actions$.stream = hot('a', { a: new LoginSuccessAction(payload) });
    effects.loginSuccess$.subscribe(() => {}, () => fail());
    expect(routerService.navigate).toHaveBeenCalledWith(['/']);
  }));
});
