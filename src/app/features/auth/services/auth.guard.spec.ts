import { TestBed, async, inject } from '@angular/core/testing';

import { AuthGuard } from './auth.guard';
import { cold } from 'jasmine-marbles';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { MockStore } from 'app/shared/test-utils';

describe('AuthGuard', () => {
  const mockRouter = { navigate: () => null };
  let guard: AuthGuard;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthGuard,
        { provide: Router, useValue: mockRouter },
        {provide: Store, useClass: MockStore }
      ]
    });
    guard = TestBed.get(AuthGuard);
  });

  it('should be created', inject([AuthGuard], (guardInjected: AuthGuard) => {
    expect(guardInjected).toBeTruthy();
  }));

  // todo (yca): test should cancel route if user is not authenticated
  xit('should cancel route if user is not authenticated', () => {
    const expected = cold('(a|)', { a: false });
    expect(guard.canActivate(null, null)).toBeObservable(expected);
  });

  xit('should go to route if user is not authenticated', () => {
    // const action = new LoginSuccess();
    // store.dispatch(action);
    // const expected = cold('(a|)', { a: true });
    // expect(guard.canActivate(null, null)).toBeObservable(expected);
  });

  xit('should redirect user to login if not authenticated', () => {

  });
});
