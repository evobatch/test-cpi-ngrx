import 'rxjs/add/observable/of';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AuthState } from 'app/features/auth/auth.reducers';
import { IAuthInfo } from 'app/models/auth-info.model';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LoginService {
  authInfo = <IAuthInfo>null;
  url = `${environment.apiRoot}/login`;
/**
 * Login Service Constructor
 * @param http HttpClient Service
 * @param router Angular Router
 * @param store ngrx "auth" store
 */
  constructor(
    private http: HttpClient,
    private router: Router,
    private store: Store<AuthState>
  ) { }

/**
 * This will call http service to login and receive a token
 * @param email User e-mail
 * @param password User password
 * @returns `IAuthInfo` containing session token
 */
  public login(email: string, password: string): Observable<IAuthInfo> {
    return this.http
        .post<IAuthInfo>(this.url, { email: email && email.trim(), password: password && password.trim() });
  }

  /**
   * This will call http service to logout (invalidate the current session)
   * @returns Observable to wait end of call.
   */
  public logout():  Observable<boolean> {
    // fake implementation
    return Observable.of(true);
  }

}
