import 'rxjs/add/operator/take';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { LoginService } from './login.service';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { MockStore } from 'app/shared/test-utils';

describe('LoginService', () => {
  // mock http requests
  let backend: HttpTestingController;
  // service instance
  let service: LoginService;
  const mockRouter = { navigate: () => null };
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        LoginService,
        {provide: Store, useClass: MockStore },
        { provide: Router, useValue: mockRouter }
      ]
    });

    service = TestBed.get(LoginService);
    backend = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    backend.verify();
  });

  describe('login()', () => {
    it('should be created', () => {
      expect(service).toBeTruthy();
    });

    it('should login successful', (done) => {
      service.login('peter@klaven', 'cityslicka')
        .take(1)
        .subscribe(data => {
          expect(data).toEqual(jasmine.objectContaining({ token: 'QpwL5tke4Pnpja7X' }));
          done();
        });

      backend
        .expectOne({ url: service.url, method: 'POST' })
        .flush({ token: 'QpwL5tke4Pnpja7X' });
    });

    it('should failed to login with a bad request', (done) => {
      const expected = { error: 'Missing password' };

      service.login('peter@klaven', null)
        .take(1)
        .subscribe(data => fail('should have failed with 400'),
          error => {
            expect(error.status).toEqual(400);
            expect(error.statusText).toEqual('Bad Request');
            done();
          });

      backend
        .expectOne(service.url)
        .flush(expected, { status: 400, statusText: 'Bad Request' });
    });
  });

  describe('logout()', () => {
    xit('should call the webservice', (done) => {
        // todo (yca):logout should call the webservice  with a real implementation
    });
  });

});
