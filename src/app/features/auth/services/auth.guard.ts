import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import * as fromAuth from '../auth.reducers';
import { Store, select } from '@ngrx/store';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/map';
// import {map, take } from 'rxjs/operators';
// import * as fromRouter from 'app/shared/router.actions';
@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private store: Store<fromAuth.AuthState>) {}

    /**
     * triggered for each route with `canActivate` property.
     * It will be called before route activation.
     * A falsy result will cancel navigation to the route and prevent unauthenticated user to continue navigation.
     * In this case, the user will be redirected to the login page.
     * @param next Route to activate
     * @param state current router state
     */
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.store
      .select(fromAuth.getAuthenticated)
      .map(isAuthenticated => {
        if (!isAuthenticated) {
          this.router.navigate(['/login']);
        }
        return isAuthenticated;
      })
      .take(1);
  }
}
