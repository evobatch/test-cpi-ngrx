import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileComponent } from './profile.component';
import { MaterialModule } from 'app/shared/material-module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Store } from '@ngrx/store';
import { MockStore } from 'app/shared/test-utils';
import { Router } from '@angular/router';

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;
  const mockRouter = { navigate: () => null };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MaterialModule,
        NoopAnimationsModule],
      declarations: [ProfileComponent],
      providers: [
        { provide: Router, useValue: mockRouter },
        { provide: Store, useClass: MockStore }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
