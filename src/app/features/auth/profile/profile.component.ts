import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AuthState } from '../auth.reducers';
import { Store } from '@ngrx/store';
import * as fromAuth from '../auth.actions';
import * as auth from '../auth.reducers';
import { IAppUser } from 'app/models/app-user.model';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  destroy = false;
  user$: Observable<IAppUser>;
  authenticated$: Observable<boolean>;
  constructor(
    private store: Store<AuthState>,
    private router: Router) { }

  ngOnInit() {
    this.user$ = this.store
      .select(auth.getUser)
      .takeWhile(() => !this.destroy);

    this.authenticated$ = this.store
      .select(auth.getAuthenticated)
      .takeWhile(() => !this.destroy);
  }

  ngOnDestroy() {
    this.destroy = true;
  }


  logout() {
    this.store.dispatch(new fromAuth.LogoutAction());
  }
}
