import 'rxjs/add/observable/empty';

import { DebugElement } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

export const ButtonClickEvents = {
  left:  { button: 0 },
  right: { button: 2 }
};
/**
 * Useful click function
 * @param el debug element to click
 * @param eventObj click left or right?
 */
export function click(el: DebugElement | HTMLElement, eventObj: any = ButtonClickEvents.left): void {
  if (el instanceof HTMLElement) {
    el.click();
  } else {
    el.triggerEventHandler('click', eventObj);
  }
}


/**
 * <https://github.com/ngrx/store/issues/128>
 * Standard mockstore that can be used in unittests to mock a @ngrx/store
 */
export class MockStore<T> {

  reducers = new Map<string, BehaviorSubject<any>>();

  /**
   * simple solution to support selecting/subscribing to this mockstore as usual.
   * @param name reducer name
   * @returns {undefined|BehaviorSubject<any>}
   */
  select(name: string) {
    if (!this.reducers.has(name)) {
      this.reducers.set(name, new BehaviorSubject({}));
    }
    return this.reducers.get(name);
  }

  /**
   * used to set a fake state
   * @param reducerName name of your reducer
   * @param data the mockstate you want to have
   */
  mockState(reducerName, data) {
    this.select(reducerName).next(data);
  }

  dispatch(data: any) {
    // spy me
  }
}

export class TestActions extends Actions {
  constructor() {
    super(Observable.empty());
  }

  set stream(source: Observable<any>) {
    this.source = source;
  }
}

export function getActions() {
  return new TestActions();
}
