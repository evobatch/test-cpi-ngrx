import { Params, RouterStateSnapshot } from '@angular/router';
import { RouterStateSerializer } from '@ngrx/router-store';

// https://github.com/ngrx/platform/blob/v4.1.1/example-app/app/shared/utils.ts

export interface RouterStateUrl {
  url: string;
  queryParams: Params;
}
/**
 * Simple Custom Router State serializer to be used with ngrx router-store.
*/
export class CustomRouterStateSerializer implements RouterStateSerializer<RouterStateUrl> {
  serialize(routerState: RouterStateSnapshot): RouterStateUrl {
    return { url: routerState.url, queryParams: routerState.root.queryParams };
  }
}
