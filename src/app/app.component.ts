import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
/**
 * AppComponent is the main app component.
 * contains main `router-outlet`
 */
export class AppComponent {

  constructor() { }

}
