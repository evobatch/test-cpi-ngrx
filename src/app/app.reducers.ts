import { routerReducer, RouterReducerState } from '@ngrx/router-store';
import { ActionReducer, ActionReducerMap, MetaReducer } from '@ngrx/store';
import { RouterStateUrl } from 'app/shared/custom-router-state-serializer';
import { environment } from 'environments/environment';
import { storeFreeze } from 'ngrx-store-freeze';


/**
 * Main App State
 */
export interface AppState {
  router: RouterReducerState<RouterStateUrl>;
}

/**
 * App reducers to pass into App StoreModule configuration.
 */
export const reducers: ActionReducerMap<AppState> = {
  router: routerReducer
};

/**
 * Meta Reducer responsible to log all actions.
 * @param reducer current reducer callback
 */
export function logger(reducer: ActionReducer<AppState>): ActionReducer<AppState> {
  return function(state: AppState, action: any): AppState {
    console.log('state', state, 'action', action);
    return reducer(state, action);
  };
}

/**
 * Array of MetaReducer to pass into App StoreModule configuration.
 * It will use storeFreeze as a meta-reducer in development mode to
 * throw on mutating state.
 * see <https://github.com/brandonroberts/ngrx-store-freeze>
 */
export const metaReducers: MetaReducer<AppState>[] = !environment.production
  ? [storeFreeze, logger] : [];
