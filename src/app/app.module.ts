import { FullscreenOverlayContainer, OverlayContainer } from '@angular/cdk/overlay';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EffectsModule } from '@ngrx/effects';
import { RouterStateSerializer, StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from 'environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { metaReducers, reducers } from './app.reducers';
import { AuthModule } from './features/auth/auth.module';
import { BooksModule } from './features/books/books.module';
import { HeaderComponent } from './features/header/header.component';
import { PageNotFoundComponent } from './features/page-not-found/page-not-found.component';
import { CustomRouterStateSerializer } from './shared/custom-router-state-serializer';
import { MaterialModule } from './shared/material-module';


@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    HeaderComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    EffectsModule.forRoot([]),
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreRouterConnectingModule.forRoot({ stateKey: 'router' }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    HttpClientModule,
    MaterialModule,
    FlexLayoutModule,
    AuthModule.forRoot(),
    BooksModule.forRoot()
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'en-US' },
    {provide: OverlayContainer, useClass: FullscreenOverlayContainer},
    { provide: RouterStateSerializer, useClass: CustomRouterStateSerializer }
  ],
  bootstrap: [AppComponent]
})
/**
 * This is the main app module.
 * It boostraps main component `AppComponent`.
 */
export class AppModule { }
