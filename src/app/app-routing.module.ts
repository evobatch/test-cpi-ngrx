import { NgModule } from '@angular/core';
import { Route, RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './features/auth/services/auth.guard';
import { PageNotFoundComponent } from './features/page-not-found/page-not-found.component';

/**
 * Main Application Routes
*/
export const routes: Routes = <Array<Route>>[
  { path: '', redirectTo: '/books', pathMatch: 'full' },
  { path: '', loadChildren: './features/books/books.module#BooksRootModule', canActivate: [AuthGuard] },
  { path: '', loadChildren: './features/auth/auth.module#AuthRootModule' },
  { path: '404', component: PageNotFoundComponent },
  { path: '**', redirectTo: '/404' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
/**
 * this class only contains router import with routes definition.
 */
export class AppRoutingModule { }


