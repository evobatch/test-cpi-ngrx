# TestCpi

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.0.

* Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
* Use ng CLI tool to scaffold code. example: `ng generate component component-name`
* Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.
* Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
* Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/). Before running the tests make sure you are serving the app via `ng serve`.
* To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## First Installation

```bash
npm install
npm run docgen
```

To see auto generated documentation `npm run docserve`

## References

* Testing with angular <https://angular.io/guide/testing>
* How to use ngrx <https://github.com/ngrx/platform/blob/master/example-app>



## Miscellaneous

* <https://codecraft.tv/courses/angular/unit-testing/routing/>
* <https://github.com/Microsoft/vscode-recipes/tree/master/Angular-CLI>

If you are missing error messages with angular testing, you should consider disabling source maps when running Angular tests :

```bash
ng test --sourcemaps=false
```

Prefer to use local package from node_modules to avoid version collision

```bash
$(npm bin)/ng
```

Kill task using 4200 port.

```cmd
netstat -ano | findstr :4200
taskkill /PID 15940 /F
```

Delete Git Merge remaining files

```bash
find . -name \*.orig
find . -name \*.orig -delete
```
